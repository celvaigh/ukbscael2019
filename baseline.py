#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#oarsub -I -l /cpu=2,walltime=2:00:00 -p "mem_cpu>='48179'"
import heapq
import os,sys,time,math
import numpy as np
from gensim.models import KeyedVectors,FastText
import bz2
import re
import string
import operator
from operator import itemgetter
from helper import *
import concurrent.futures
from multiprocessing import Pool
import threading

import treetaggerwrapper
from datasketch import LeanMinHash,MinHash, MinHashLSHForest,MinHashLSHEnsemble
import pickle
from nltk.stem.snowball import SnowballStemmer
from sklearn.feature_extraction.text import CountVectorizer
from itertools import groupby
import itertools
from joblib import dump, load
semID = threading.Semaphore()
semNIL = threading.Semaphore()
PARALLEL=True
ID=1
NIL=1
from multiprocessing import  Value,Process,Pipe
v1 = Value('i', 1)
v2 = Value('i', 1)

permutations=128
print("imports finished")

import argparse

parser = argparse.ArgumentParser(description='Description of your program')
parser.add_argument('-i','--igrida', help='using igrida or not',nargs='?',const=False, required=False)
args = vars(parser.parse_args())
igrida=True

print(args["igrida"])

if args["igrida"]:igrida=args["igrida"]

if igrida:os.environ["JAVA_HOME"]="/usr/lib/jvm/java-1.8.0-openjdk-amd64/"
else:os.environ["JAVA_HOME"]="/usr/lib/jvm/java-1.8.0-openjdk/"


fbt.configure(home='freebase-tools-1.2.0/', config='freebase-tools-1.2.0/config.dat')
fbi = fbt.FreebaseIndex()
print("lucene index ok")
#exit()
#with open("mids2wikipedia.index","rb") as f:lsh=pickle.load(f)

	
def getAccro(key,idx):
	try:return abrdic[key.lower()][idx].lower()
	except:return key

def store(predictions,output="tac2018runs/tac2017.run"):
	with open(output,"w") as f:f.write("\n".join(predictions))

	
def updateabrdic():
	with open("accronymes.csv") as f:tmp=f.read().split("\n")
	abrdic={}
	for i in tmp:j=i.split(";");abrdic[j[0]]=j[1:]	
	return abrdic


import string
import wikipedia
punc=string.punctuation+" "
def remove_punc(s,punc):return s.lower().translate(string.maketrans("",""), punc)
def get_article(aid):return wikipedia.page(pageid=aid).title

def convertType(typ):
	if typ=="PER":typ="f_people.person"
	elif typ=="GPE" or typ=="LOC" or typ=="FAC":typ="f_location.location"
	elif typ=="ORG":typ="f_organization.organization"
	return typ
	

	
def generate_candidate(mention):
	global freebasenames
	global wikipages
	global generatedcands
	es,ps=[],[]
	try:return generatedcands[mention]
	except:pass
	try:es,ps=crosswiki[remove_punc(getAccro(mention,0),punc)]#candidates generation from crosswiki
	except:
		#Mention not in crosswiki use wikipedia then
		try:wcands=wikisearch[getAccro(mention,0)]
		except:
			try:wcands=wikipedia.search(getAccro(mention,0))
			except:
				generatedcands[mention]=[es,ps]
				return [es,ps]
			wikisearch[getAccro(mention,0)]=wcands
		for wcand in wcands:
				try:wid=wikipages[wcand]
				except:
					try:wid=wikipedia.page(wcand).pageid
					except:continue#wid="NIL"
					wikipages[wcand]=wid
					try:p=cm[(mention.lower(),wcand.lower())]
					except:p=0
					ps+=[p]
					es+=[wid]	
				try:freebasenames[wids2mids[wid]]=wcand
				except:pass
	cand=[es,ps]	
	generatedcands[mention]=cand
	return cand
def generate_wikipedia(mention):
	global freebasenames
	global wikipages
	global generatedcands
	es,ps=[],[]
	try:return generatedcands[mention]
	except:pass
	#Mention not in crosswiki use wikipedia then
	try:wcands=wikisearch[getAccro(mention,0)]
	except:
		try:wcands=wikipedia.search(getAccro(mention,0))
		except:
			generatedcands[mention]=[es,ps]
			return [es,ps]
		wikisearch[getAccro(mention,0)]=wcands
	for wcand in wcands:
			try:wid=wikipages[wcand]
			except:
				try:wid=wikipedia.page(wcand).pageid
				except:continue
				wikipages[wcand]=wid
				try:p=cm[(mention.lower(),wcand.lower())]
				except:p=0
				ps+=[p]
				es+=[wid]	
			try:freebasenames[wids2mids[wid]]=wcand
			except:pass
	cand=[es,ps]	
	generatedcands[mention]=cand
	return cand	
def generate_candidate_baseKB(mention,mid):
	global freebasenames
	global wikipages
	global generatedcands
	ss=0
	es,ps=[],[]
	try:return generatedcands[mid]
	except:pass
	#Mention not in crosswiki use wikipedia then
	try:wcands=fbi.retrieve(getAccro(mention,0),maxHits=500)
	except:
		generatedcands[mid]=[es,ps]
		return [es,ps]
	#wikisearch[getAccro(mention,0)]=wcands
	for wcand in wcands:
			try:name,score,cmid=wcand["rs_label"],wcand["_score"],wcand["subject"]
			except:pass
			#wikipages[wcand]=wid
			try:p=cm[(mention.lower(),name.lower())]
			except:p=0
			ps+=[p]
			es+=[cmid]	
	cand=[es,ps]	
	generatedcands[mention]=cand
	return cand
freebasenames={}
freebasetype={}	
wikipages={}
wikisearch={}
generatedcands={}
def make_mention(sentence,aida=False):
	doci=0
	doc=[]
	mentions=[]
	dicmentions={}
	global freebasenames
	global freebasetype
	goodgen=0
	while True:
		
		if doci>=len(sentence) or len(sentence)<1:break
		s1=sentence[doci]
		#print(doci)
		if aida:mention,mid=s1 #aida corpus
		else:mention,mid,typ=s1[2],s1[4],s1[5] #tac kb corpus
		try:doc+=[dicmentions[mid]];doci+=1;continue
		except:pass
		cand=generate_candidate(mention)#generate_candidate(mention)
		mids=cand[0]
		mentions=[]
		#if len(cand[0])==0:mentions=[[mention,"NIL","NIL","NIL",0,0,0,0,0,0,0,0]]#candidate found
		for c in range(len(cand[0])):
			mrel=cand[1][c]
			sim=0
			midc=None
			try:midc=wids2mids[cand[0][c]]
			except:mentions+=[[mention,"NIL","NIL",mid,sim,mrel,0,0,0,0,0,0]];continue
			try:cname=freebasenames[midc]
			except:
				try:cname=get_article(cand[0][c])
				except:
					cname="NIL"
					#freebasenames[midc]=cname
					#mentions+=[[mention,cname,"NIL",mid,sim,mrel,0,0,0,0,0,0]]
					continue
				freebasenames[midc]=cname
			try:sim=1 - spatial.distance.cosine(wtranse["_".join(cname.lower().split(" "))],wtranse["_".join(mention.lower().split(" "))])
			except:pass
			meanErel=0
			m1,m2,m3=0,0,0
			meanErel,meanErel2,meanErel3=0,0,0
			maxt,maxt2,maxt3=[],[],[]
			
			#Entities confidence score computation
			for j in range(len(sentence)):
				if doci==j:continue
				s2=sentence[j]
				if aida:mention1,mid1=s2
				else:mention1,mid1,typ1=s2[2],s2[4],s2[5]
				cand1=generate_candidate(mention1)
				tmp,tmp2,tmp3,m1,m2,m3,t,t2,t3=0,0,0,0,0,0,0,0,0
				for c1 in range(len(cand1[0])):
					try:midc1=wids2mids[cand1[0][c1]]
					except:continue
					###########Path length 1#################
					try:t=entityRel[(midc,midc1)]#+entityRel[(midc1,midc)])/2.0
					except:pass
					try:t+=entityRel[(midc1,midc)]
					except:pass
					t=t/2.0
					tmp+=t
					m1=max(t,m1)
					###########Path length 2#################
					try:t2=twoedges[(mid2numsdic[midc],mid2numsdic[midc1])]#+twoedges[(mid2numsdic[midc1],mid2numsdic[midc])])/2.0
					except:pass
					try:t2+=twoedges[(mid2numsdic[midc1],mid2numsdic[midc])]
					except:pass
					t2=t2/2.0
					tmp2+=t2
					m2=max(t2,m2)
					###########Path length 3#################
					try:t3=threeedges[(mid2numsdic[midc],mid2numsdic[midc1])]
					except:pass
					try:t3+=threeedges[(mid2numsdic[midc1],mid2numsdic[midc])]
					except:pass
					#if t3>0:print("youupi",t3);exit()
					t3=t3/2.0
					tmp3+=t3
					m3=max(t3,m3)
				meanErel+=tmp;meanErel2+=tmp2;meanErel3+=tmp3
				maxt+=[m1];maxt2+=[m2];maxt3+=[m3]
			y=int(mid==midc)
			ftyp=0
			####Here we check the types###
			if not y:
				if "NIL" in mid or midc==None or "NIL" in midc:ftyp=0
				else:
					try:ftyp=freebasetype[mid]
					except:
						try:ftyp=int(len(set(fbi.fetch(mid)["r_type"]).intersection(fbi.fetch(midc)["r_type"]))!=0)
						except:ftyp=0
						freebasetype[mid]=ftyp
			else:ftyp=1
			maxa123p1=heapq.nlargest(min(3,len(maxt)),maxt)
			if len(maxa123p1)<3:maxa123p1+=(3-len(maxa123p1))*[0]
			maxa123p2=heapq.nlargest(min(3,len(maxt2)),maxt2)
			if len(maxa123p2)<3:maxa123p2+=(3-len(maxa123p2))*[0]
			maxa123p3=heapq.nlargest(min(3,len(maxt3)),maxt3)
			if len(maxa123p3)<3:maxa123p3+=(3-len(maxa123p3))*[0]
			mentions+=[[mention,cname,midc,mid,sim,mrel,meanErel]+maxa123p1+[meanErel2]+maxa123p2+[meanErel3]+maxa123p3+[ftyp,y]]
		dicmentions[mid]=mentions
		doc+=[mentions]
		doci+=1
	return doc	


freebasetype={}		

def getConll2003(filename):
	doc,docs=[],[]
	with open(filename,"r") as f:lines=f.read().split("\n")
	for l in lines:
		if "-DOCSTART-" in l:
			if len(doc)>0:docs+=[doc]
			doc=[]
			continue
		#if "m." not in l:continue
		try:
			_,m,_,wid,mid=l.split("\t")
			doc+=[[m.replace("_"," "),mid.replace("/m/","m.")]]
		except:continue
	docs+=[doc]
	return docs
	
def getTAC2K814(filename):
	with open(filename,"r") as f:lines=[i.split("\t") for i in f.read().split("\n")]
	return itertools.groupby(lines,key=lambda x:x[3])
	
def getRaws(filename):
	with open(filename,"r") as f:lines=[i.split("\t")[:8] for i in f.read().split("\n") if len(i.split("\t"))>5]
	lines=sorted(lines,key=lambda x: (x[3].split(":")[0],int(x[3].split(":")[1].split("-")[0])))
	return itertools.groupby(lines,key=lambda x:x[3].split(":")[0])
	
def makesample(it):
	mentions=[]
	for i in it:
		mentions+=[["Doc"]]
		mentions+=[m for m in i[1]]
	return mentions+[["Doc"]]
	
def make_dataset(dataset):
	documents=[]
	document=[]
	for w in dataset:
		if "Doc" in w:
			if len(document)!=0:documents+=[document]
			document=[]
		else:
			if len(w)<8:continue
			if len(w)>8:w=w[:8]
			document+=[w]	
	return documents
	
def getData(filename):
	with open("tac2015.eval.clean.chunks.tsv") as f:data=[i.split()for i in f.read().split("\n") if len(i)>0]
	return data
def word2raw(word):
	raw=""
	for i in word:raw+="\t".join(map(str,i))+"\n"
	return raw
def doc2raw(sentence):
	raw=""
	for i in sentence:raw+="Mention\n"+word2raw(i)
	return raw
def dataset2raw(dataset):
	raw=""
	for i in dataset:raw+="DOC\n"+doc2raw(i)
	return raw
def dataset2file(dataset,filename):
	with open(filename,"w") as f:f.write(dataset2raw(dataset))
	
loadDic=True
if loadDic:
	print("loading dicts...")
	sys.stdout.flush()
	punc=string.punctuation+" "
	#mids2wids=np.load("basekb.mids2wids.npy").item()
	print("basekb.mids2wids loadded");sys.stdout.flush()
	wids2mids=np.load("basekb.wids2mids.npy",allow_pickle=True).item()
	print("basekb.mids2wids loadded");sys.stdout.flush()
	twoedges=np.load("data/twoedgespaths.hamacher.0.npy",allow_pickle=True).item()
	threeedges=np.load("data/threeedgespaths.csv.hamacher.0.npy",allow_pickle=True).item()
	print("two edges and three ok")
	mid2numsdic=np.load("data/mid2numsdic.npy",allow_pickle=True).item()
	print("mid2num ok")
	abrdic=updateabrdic()
	crosswiki=np.load("crosswikis.pruned.npy",allow_pickle=True).item()
	print("crosswikis loaded");sys.stdout.flush()
	entityRel=np.load("data/entityCommonness2.npy",allow_pickle=True).item()
	print("entityCommonness loaded");sys.stdout.flush()
	corefdic={}
	wtranse=KeyedVectors.load_word2vec_format("models/skipgram.s100.w5.enwikianchor15G.cleaned.bin", binary=True,unicode_errors="ignore")
	print("embedding model ok")
	cm=np.load("commonness4.npy",allow_pickle=True).item()
	print("dicts loaded")
	sys.stdout.flush()


	
def buildcorpus(filename,out,maker=0,aida=False):
	print("loading corpus...");sys.stdout.flush()
	predictions=[]
	if aida:
		print("aida");sys.stdout.flush()
		dataset=getConll2003(filename)
	else:
		#dataset=make_dataset(makesample(getTAC2K814(filename)))
		dataset=make_dataset(makesample(getRaws(filename)))
	print("computing features...");sys.stdout.flush()
	n=1
	t1=time.time()
	t0=time.time()
	print(len(dataset))
	for i in dataset:
		print(len(i))
		predictions+=[make_mention(i,aida)]
		print("toook ",time.time()-t1,"idx",n)
		#print("toook ",time.time()-t0)
		sys.stdout.flush()
		t1=time.time()
		n+=1
	print("storing features...")
	dataset2file(predictions,out)
	print("total toook ",time.time()-t0)
	print(out)


def makeAida(exp):
	buildcorpus("aida-yago2-dataset/aida.test.tsv","aida-yago2-dataset/aida."+exp+".test.tsv",0,True)
	buildcorpus("aida-yago2-dataset/aida.valid.tsv","aida-yago2-dataset/aida."+exp+".valid.tsv",0,True)
	buildcorpus("aida-yago2-dataset/aida.train.tsv","aida-yago2-dataset/aida."+exp+".train.tsv",0,True)
	buildcorpus("N3/Reuters-128.tsv","N3/Reuters-128."+exp+".tsv",0,True)
	buildcorpus("N3/RSS-500.tsv","N3/RSS-500."+exp+".tsv",0,True)
	buildcorpus("wikipedia/wikipedia.tsv","wikipedia/wikipedia."+exp+".tsv",0,True)
def makeTAC(exp):
	buildcorpus("ENG2017_EVAL.tab","tac17."+exp+".tsv",0,False)
	buildcorpus("ENG2016_EVAL.tab","tac16."+exp+".eval.tsv",0,False)
	

#makeAida("exp15.hamacher.0.p1.p2.p3")
#makeTAC("exp15.hamacher.0.p1.p2.p3")

print("#################  the END ###########################")
