# UKBSCAEL2019

Collective Entity Linker with  RDF KB for [iCoda project](https://project.inria.fr/icoda/)

## Local scores
```python
 Cosine similarity : Skip-gram
 
 Popularity : Cross-Wiki + Wikipedia
 ```
 
## Relatedness
```python
 WSRM
 
 WSRMP_m
 ```
## Linking strategy
```python
 We adopt a supervised approach, where a binary classifier is trained to predict whether a mention and a candidate entity are related (1) or not (0).
 The features used are, the some and the three greatest values of relatedness scores within one document.
```
## Datasets

- [TAC EDL 2016-2017](https://www.ldc.upenn.edu/)
- [N3-Collection](https://github.com/dice-group/n3-collection/)
   - N3/Reuters-128.ttl
   - N3/RSS-500.ttl
- [AIDA](http://resources.mpi-inf.mpg.de/yago-naga/aida/download/aida-yago2-dataset.zip) : Follow the instructions in the README
   - AIDA-train
   - AIDA-testa
   - AIDA-testb
 