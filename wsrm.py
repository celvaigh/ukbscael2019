import gzip,math
import matplotlib
matplotlib.use('Agg')
import pylab as plt
import numpy as np
import wikipedia
import os,sys,time
import pandas as pd
import scipy.sparse

class Metrics(object):
	def __init__(self, path,ontolgypath=None,plotname=None,output="output.npy",verbose=0):
		self.path=path
		self.verbose=verbose
		self.output=output
		self.plotname=plotname
		self.dic={}
		self.entities={}
		self.ontolgypath=ontolgypath
		self.name="Metric"
	def compute(self):return None
	def plot(self):return None

class WSRM(Metrics):

	def compute(self):
		if os.path.isfile(self.path):files_names =[self.path]
		else:files_names = [self.path+f for f in os.listdir(self.path)]
		if self.verbose:print("starting ...")
		if self.ontolgypath:subP={}
		t00=time.time()
		facts=0
		try:
			with open(self.ontolgypath) as f:
				for i in f: 
					try:subp,r,p,_=i.split()
					except:continue
					if "ressource:" in subp:subp=subp.replace("ressource:","<")+">"
					if "ressource:" in p:p=p.replace("ressource:","<")+">"
					if r!="rdfs:subPropertyOf" or (r=="rdfs:subPropertyOf" and subp==p):continue
					try:subP[subp]+=1
					except:subP[subp]=1
		except:pass
		for filename in files_names:
			t0=time.time()
			if self.verbose:
				print("procedding file :",filename)
				sys.stdout.flush()
			if ".gz" in filename:
				with gzip.open(filename,"rb") as f:
					for i in f:
						try:s,r,o,_=i.split()
						except:continue
						s,o=s.rsplit("/")[-1][:-1],o.rsplit("/")[-1][:-1]
						try:self.dic[(s,o)]+=1
						except:self.dic[(s,o)]=1
						try:self.dic[(s,o)]+=subP[r]
						except:pass
						try:self.entities[s]+=1
						except:self.entities[s]=1
						try:self.entities[o]+=1
						except:self.entities[o]=1
			else:
				with open(filename) as f: 
					for i in f:
						try:s,r,o,_=i.split();facts+=1
						except:continue
						
						if "rdf:type" in r:continue
						if "/" in s:s=s.rsplit("/")[-1]
						if "/" in o:o=o.rsplit("/")[-1]
						for c in ["<",">","ressource:"]:s,o=s.replace(c,""),o.replace(c,"")
						#if "Herwig_Aldenhoven" in i:print(i,o,s)
						#s,o=s.replace(">",""),o.replace(">","")
						#s,o=s.replace("ressource:",""),o.replace("ressource:","")
						try:self.dic[(s,o)]+=1
						except:self.dic[(s,o)]=1
						try:self.entities[s]+=1
						except:self.entities[s]=1
						try:self.entities[o]+=1
						except:self.entities[o]=1
			if self.verbose:print("toooook : ",time.time()-t0)
		wsrm={}
		if self.verbose:print("commonness : ",len(self.dic))
		if self.verbose:print("entities : ",len(self.entities))
		if self.verbose:print("facts : ",facts)
		for i in self.dic:wsrm[i]=float(self.dic[i])/self.entities[i[0]]
		np.save(self.output,wsrm)
	def plot(self):
			width=1.0
			if self.verbose:print("ploting distribution of links between entity pairs...")
			#plt.title('Distribution of links between entity pairs in BaseKB')
			plt.xlabel('Entity pairs links count')
			plt.ylabel('Occurrence')
			plt.hist(self.dic.values(),color="g")
			#plt.gca().set_xscale("log")
			plt.gca().set_yscale("log")
			plt.savefig(self.plotname)
			if self.verbose:print("#### End ###")
def basekbwsrm():
		basekbwsrm=WSRM('ldcdata/LDC2015E42_TAC_KBP_Knowledge_Base_II-BaseKB/data/',plotname="figures/BaseKBDistrib.png",output="Dicts/BaseKB.wsrm.npy",verbose=1)
		basekbwsrm.compute()
		basekbwsrm.plot()
def yagowsrm(filename,_plotname,_output):
		yago=WSRM(filename,ontolgypath="",plotname=_plotname,output=_output,verbose=1)
		yago.compute()
		yago.plot()
if __name__ == "__main__":
    basekbwsrm()
