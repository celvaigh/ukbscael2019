import sys,math
import random
import numpy as np
import pandas as pd
def ttest(filename):
	df=pd.read_csv(filename,sep=";")
	aida=map(float,list(df["AIDA"]))
	pboh=map(float,list(df["PBoH"]))
	MeEL=map(float,list(df["MeEL"]))
	print(min(MeEL));exit()
	d=[]
	n=len(aida)
	#for i in range(n):d+=[pboh[i]-aida[i]]#aida vs pboh
	for i in range(n):d+=[aida[i]-pboh[i]]#aida vs our method
	m=sum(d)/n
	s=0
	for i in range(n):s+=d[i]**2-m**2
	s=math.sqrt(s/(n-1))
	t=m/(s/math.sqrt(n))
	print(n,m,t,s)
	
ttest("N3/gerbil.reuters128.aida.pboh.tsv")
